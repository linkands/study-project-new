module.exports = (Targetables, targets) => {
    const targetables = Targetables.using(targets);

    const peregrineContextProviderTarget = targetables.reactComponent(
        '@magento/peregrine/lib/PeregrineContextProvider/peregrineContextProvider.js'
    );

    peregrineContextProviderTarget.addImport(
        "SignInContextProvider from 'src/context/signIn'"
    );
};
