module.exports = (Targetables, targets) => {
    const targetablesReact = Targetables.using(targets);

    const useProductTalon = targetablesReact.reactComponent(
        '@magento/peregrine/lib/talons/RootComponents/Product/useProduct.js'
    );

    useProductTalon.spliceSource({
        before: "from './product.gql';",
        insert: "from 'src/talons/RootComponents/Product/product.gql';",
        remove: 21
    });
};
