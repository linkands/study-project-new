module.exports = (Targetables, targets) => {
    const targetables = Targetables.using(targets);

    const useAccountTriggerTarget = targetables.module(
        '@magento/peregrine/lib/talons/Header/useAccountTrigger.js'
    );

    useAccountTriggerTarget.insertAfterSource(
        'import { useCallback',
        ', useEffect'
    );

    useAccountTriggerTarget.insertAfterSource(
        "import { useDropdown } from '@magento/peregrine/lib/hooks/useDropdown';",
        "import { useSignInContext } from 'src/context/signIn';"
    );

    useAccountTriggerTarget.insertAfterSource(
        '} = useDropdown();',
        'const {menuOpened} = useSignInContext()'
    );

    useAccountTriggerTarget.insertAfterSource(
        '}, [setAccountMenuIsOpen]);',
        'useEffect(() => {setAccountMenuIsOpen(menuOpened)}, [menuOpened])'
    );
};
