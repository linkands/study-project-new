module.exports = (Targetables, targets) => {
    const targetables = Targetables.using(targets);

    const MainComponent = targetables.module(
        '@magento/venia-ui/lib/components/Main/main.js'
    );

    // Insert a console log message in the source
    MainComponent.insertAfterSource(
        'const Main = props => {',
        'console.log("Hello World");'
    );
};
