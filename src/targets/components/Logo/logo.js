module.exports = (Targetables, targets) => {
    const targetables = Targetables.using(targets);

    const LogoComponent = targetables.reactComponent(
        '@magento/venia-ui/lib/components/Logo/logo.js'
    );

    const oldSvgImport = "from './VeniaLogo.svg'";

    LogoComponent.spliceSource({
        before: oldSvgImport,
        insert: "from 'src/targets/components/Logo/doom.svg';",
        remove: oldSvgImport.length
    });

    LogoComponent.spliceSource({
        before: 'height: 18',
        insert: 'height: 80',
        remove: 10
    });
};
