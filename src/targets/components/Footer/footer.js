module.exports = (Targetables, targets) => {
    const targetables = Targetables.using(targets);

    const FooterComponent = targetables.reactComponent(
        '@magento/venia-ui/lib/components/Footer/footer.js'
    );

    const oldDataRef = "from './sampleData'";
    const newDataRef = "from 'src/components/Footer/sampleData'";
    const oldImportRef = 'const { copyrightText } = talonProps;';

    FooterComponent.spliceSource({
        before: oldDataRef,
        insert: newDataRef,
        remove: oldDataRef.length
    });

    FooterComponent.addImport("{ useSignInContext } from 'src/context/signIn'");

    FooterComponent.addImport(
        "{ useUserContext } from '@magento/peregrine/lib/context/user'"
    );

    FooterComponent.appendJSX(
        '<div className={classes.links}>',
        "<div className={classes.link}><Link to='/foo'><span className={classes.label}>Foo Demo Page</span></Link></div>"
    );

    FooterComponent.spliceSource({
        before: oldImportRef,
        insert: 'const { copyrightText, handleFooterClick } = talonProps;',
        remove: oldImportRef.length
    });

    FooterComponent.setJSXProps('<Link className={classes.link} to={path}>', {
        onClick: '{()=>handleFooterClick(text)}'
    });
};
