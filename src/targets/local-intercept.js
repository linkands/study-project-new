const MainComponent = require('./components/Main/main');
const FooterComponent = require('./components/Footer/footer');
const LogoComponent = require('./components/Logo/logo');
const peregrineContextProviderTarget = require('./peregrineContextProvider/peregrineContextProvider');
const useAccountTriggerTarget = require('./talons/Header/useAccountTrigger');
const { Targetables } = require('@magento/pwa-buildpack');

module.exports = targets => {
    MainComponent(Targetables, targets);
    FooterComponent(Targetables, targets);
    LogoComponent(Targetables, targets);
    peregrineContextProviderTarget(Targetables, targets);
    useAccountTriggerTarget(Targetables, targets);

    targets.of('@magento/venia-ui').routes.tap(routes => {
        routes.push({
            name: 'MyGreetingRoute',
            pattern: '/greeting/:who?',
            path: require.resolve('../components/GreetingPage/greetingPage.js')
        });
        routes.push({
            name: 'MyCustomComponentRoute',
            pattern: '/foo/',
            path: require.resolve('../components/FooPage/fooPage.js')
        });
        return routes;
    });

    const peregrineTargets = targets.of('@magento/peregrine');
    const talonsTarget = peregrineTargets.talons;
    talonsTarget.tap(talonWrapperConfig => {
        talonWrapperConfig.Footer.useFooter.wrapWith(
            'src/wrappers/Footer/useFooter'
        );
    });
};
