import { useSignInContext } from 'src/context/signIn';
import { useUserContext } from '@magento/peregrine/lib/context/user';

const useFooter = original => {
    return function useFooter(props, ...restArgs) {
        const { ...defaultReturnData } = original({ ...props }, ...restArgs);

        const { toggleMenu } = useSignInContext();
        const [{ isSignedIn }] = useUserContext();

        const handleFooterClick = text => {
            if (text === 'Sign In') {
                toggleMenu();
                return;
            }
            if (text === 'Order Status' && !isSignedIn) {
                toggleMenu();
                return;
            }
            if (text === 'Order Status' && isSignedIn) {
                alert('Ei tea kuidas seda teha');
                return;
            }
        };

        return {
            ...defaultReturnData,
            handleFooterClick
        };
    };
};

export default useFooter;
