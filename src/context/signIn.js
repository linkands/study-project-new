import React, { createContext, useContext, useState } from 'react';

const SignInContext = createContext();

const SignInContextProvider = props => {
    const [menuOpened, setMenuOpened] = useState(false)
    const toggleMenu = () => {
        setMenuOpened(isOpened => !isOpened)
    }

    const contextValue = {
        menuOpened,
        setMenuOpened,
        toggleMenu
    }

    return <SignInContext.Provider value={contextValue}>{props.children}</SignInContext.Provider>
}

export default SignInContextProvider
export const useSignInContext = () => useContext(SignInContext);