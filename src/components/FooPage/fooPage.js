import React from 'react';
import { gql, useQuery } from '@apollo/client';
import Gallery from '../Gallery';

const hi = {
    textAlign: 'center',
    margin: '1rem'
};
const wave = {
    ...hi,
    fontSize: '5rem'
};

const GET_ITEMS = gql`
    query {
        products(search: "", filter: { price: { to: "50" } }) {
            items {
                __typename
                id
                name
                image {
                    url
                }
                price {
                    __typename
                    regularPrice {
                        amount {
                            __typename
                            currency
                            value
                        }
                    }
                }
                sku
                small_image {
                    __typename
                    url
                }
                url_key
                url_suffix
            }
        }
    }
`;

const FooPage = () => {
    const { loading, error, data } = useQuery(GET_ITEMS);

    if (loading) return 'Loading...';
    if (error) return 'Fetching failed';

    return (
        <div>
            <h1 style={hi}>Welcome to Foo component!</h1>
            <h1 style={wave}>{'\uD83D\uDC4B'}</h1>
            <Gallery items={data.products.items} />
        </div>
    );
};

export default FooPage;
