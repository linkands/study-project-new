const accountLinks = new Map()
    .set('Account', null)
    .set('Sign In', '#')
    .set('Register', `create-account`)
    .set('Order Status', '#');

const aboutLinks = new Map()
    .set('About Us', null)
    .set('Our Story', null)
    .set('Email Signup', null)
    .set('Give Back', null);

const helpLinks = new Map()
    .set('Help', null)
    .set('Live Chat', null)
    .set('Contact Us', null)
    .set('Order Status', null);

export const DEFAULT_LINKS = new Map()
    .set('account', accountLinks)
    .set('about', aboutLinks)
    .set('help', helpLinks);

export const LOREM_IPSUM =
    'Changed text by targeting';
