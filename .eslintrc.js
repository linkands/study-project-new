const config = {
    env: {
        es6: true
    },
    plugins: ['package-json', 'react', 'react-hooks'],
    parser: 'babel-eslint',
    extends: [
        'eslint:recommended',
        'prettier',
        'plugin:package-json/recommended'
    ],
    rules: {
        'prefer-const': 'error',
        'no-console': 'off',
        'no-unused-vars': 'error',
        'react/jsx-uses-vars': 'error',
        'react/jsx-uses-react': 'error',
        'react-hooks/exhaustive-deps': 'error',
        'react-hooks/rules-of-hooks': 'error',
        'no-duplicate-imports': 'error',
        'one-var': ['error', 'never'],
        'no-undef': 'off',
        'no-useless-escape': 'off',
        'no-prototype-builtins': 'off'
    }
};

module.exports = config;
